from blog import views
from django.urls import path

app_name = "blog"

urlpatterns = [
    path("list", views.PostList.as_view(), name="post-list"),
    path(
        "post/<int:pk>",
        views.PostDetail.as_view(),
        name="post-detail",
    ),
    path(
        "post/create",
        views.PostCreateView.as_view(),
        name="post-create",
    ),
]
