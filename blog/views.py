from blog.forms import PostForm
from blog.models import Post
from django.http import HttpResponse
from django.urls import reverse
from django.views.generic import CreateView, DetailView, ListView


def home_view(request):
    return HttpResponse("<h1>Bienvenue !</h1>")


class PostList(ListView):
    """Post list view"""

    model = Post


class PostDetail(DetailView):
    """Post detail view"""

    model = Post


class PostCreateView(CreateView):
    """Post create view"""

    form_class = PostForm
    template_name = "blog/post_form.html"

    def get_success_url(self, **kwargs):
        return reverse(
            "blog:post-detail",
            kwargs={"pk": self.object.pk},
        )
