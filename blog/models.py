from django.db import models


class Post(models.Model):
    """Model for Post"""

    title = models.CharField(max_length=100)
    description = models.TextField(max_length=500, blank=True, null=True)
    author = models.ForeignKey(
        "Author",
        related_name="posts",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )

    def __str__(self):
        return self.title


class Author(models.Model):
    """Model for Author"""

    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)

    def __str__(self):
        return self.name
