from blog.models import Author, Post
from django.contrib import admin

admin.site.register(Post)
admin.site.register(Author)
